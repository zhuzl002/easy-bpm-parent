package com.pig.easy.bpm.vo.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 通知内容表
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)

@ApiModel(value="通知内容表对象", description="通知内容表")
public class MessageContentQueryVO implements Serializable {

    private static final long serialVersionUID=1L;

        
                    @ApiModelProperty(value = "通知编号")
             /**
    * 通知编号
    */
        private Long contentId;

    
                    @ApiModelProperty(value = "内容编码")
             /**
    * 内容编码
    */
        private String contentCode;

    
                    @ApiModelProperty(value = "租户编号")
             /**
    * 租户编号
    */
        private String tenantId;

    
                    @ApiModelProperty(value = "通知标题")
             /**
    * 通知标题
    */
        private String messageTitle;

    
                    @ApiModelProperty(value = "通知内容")
             /**
    * 通知内容
    */
        private String messageContent;

    
                    @ApiModelProperty(value = "通知类型编号 ’HTML‘:网页,'TEXT'：文本")
             /**
    * 通知类型编号 ’HTML‘:网页,'TEXT'：文本
    */
        private String messageTypeCode;

    
                    @ApiModelProperty(value = "通知系统")
             /**
    * 通知系统
    */
        private String messageSystemCode;

    
                    @ApiModelProperty(value = "通知平台 ")
             /**
    * 通知平台 
    */
        private String messagePlatform;

    
                    @ApiModelProperty(value = "流程编号")
             /**
    * 流程编号
    */
        private Integer processId;

    
                    @ApiModelProperty(value = "是否为默认版本 0 否，1 是")
             /**
    * 是否为默认版本 0 否，1 是
    */
        private Integer defaultFalg;

    
                    @ApiModelProperty(value = "触发事件编码")
             /**
    * 触发事件编码
    */
        private String eventCodes;

    
                    @ApiModelProperty(value = "触发事件名称")
             /**
    * 触发事件名称
    */
        private String eventNames;

    
                    @ApiModelProperty(value = "排序")
             /**
    * 排序
    */
        private Integer order;

    
                    @ApiModelProperty(value = "备注")
             /**
    * 备注
    */
        private String remarks;

    
                    @ApiModelProperty(value = "状态 1 有效 0 失效")
             /**
    * 状态 1 有效 0 失效
    */
        private Integer validState;

    
                    @ApiModelProperty(value = "操作人工号")
             /**
    * 操作人工号
    */
        private Integer operatorId;

    
                    @ApiModelProperty(value = "操作人工姓名")
             /**
    * 操作人工姓名
    */
        private String operatorName;

    
                    @ApiModelProperty(value = "创建时间")
             /**
    * 创建时间
    */
        private LocalDateTime createTime;

    
                    @ApiModelProperty(value = "更新时间")
             /**
    * 更新时间
    */
        private LocalDateTime updateTime;
    /**
    *  当前页码
    */
    private Integer pageIndex;

    /**
     * 每页展示数量
     */
    private Integer pageSize;


}
