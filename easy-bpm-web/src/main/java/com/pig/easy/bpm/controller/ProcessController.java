package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.ProcessReqDTO;
import com.pig.easy.bpm.dto.response.DynamicFormDataDTO;
import com.pig.easy.bpm.dto.response.ProcessDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.ProcessService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.ProcessVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 流程表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
@RestController
@RequestMapping("/process")
@Api(tags = "流程管理", value = "流程管理")
public class ProcessController extends BaseController {

    @Reference
    ProcessService processService;

    @ApiOperation(value = "查询流程列表", notes = "查询流程列表", produces = "application/json")
    @PostMapping("/getList")
    public JsonResult getList(@ApiParam(name = "传入流程详细信息", value = "传入json格式", required = true) @Valid @RequestBody ProcessVO processVO) {

        BestBpmAsset.isAssetEmpty(processVO);
        BestBpmAsset.isAssetEmpty(processVO.getTenantId());
        ProcessReqDTO processReqDTO = switchToDTO(processVO, ProcessReqDTO.class);

        Result<PageInfo> result = processService.getListByCondition(processReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增流程", notes = "新增流程", produces = "application/json")
    @PostMapping("/insert")
    public JsonResult insert(@Valid @RequestBody ProcessVO processVO) {

        BestBpmAsset.isAssetEmpty(processVO);
        BestBpmAsset.isAssetEmpty(processVO.getTenantId());
        BestBpmAsset.isAssetEmpty(processVO.getProcessKey());
        ProcessReqDTO processReqDTO = switchToDTO(processVO, ProcessReqDTO.class);
        processReqDTO.setOperatorId(currentUserInfo().getUserId());
        processReqDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = processService.insertProcess(processReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "修改流程", notes = "修改流程", produces = "application/json")
    @PostMapping("/update")
    public JsonResult updateProcess(@Valid @RequestBody ProcessVO processVO) {

        BestBpmAsset.isAssetEmpty(processVO);
        BestBpmAsset.isAssetEmpty(processVO.getTenantId());
        BestBpmAsset.isAssetEmpty(processVO.getProcessKey());
        ProcessReqDTO processReqDTO = switchToDTO(processVO, ProcessReqDTO.class);

        Result<Integer> result = processService.updateProcessByProcessKey(processReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "删除流程", notes = "删除流程", produces = "application/json")
    @PostMapping("/delete")
    public JsonResult delete(@Valid @RequestBody ProcessVO processVO) {

        BestBpmAsset.isAssetEmpty(processVO);
        BestBpmAsset.isAssetEmpty(processVO.getTenantId());
        BestBpmAsset.isAssetEmpty(processVO.getProcessKey());
        ProcessReqDTO processReqDTO = switchToDTO(processVO, ProcessReqDTO.class);

        processReqDTO.setOperatorId(currentUserInfo().getUserId());
        processReqDTO.setOperatorName(currentUserInfo().getRealName());
        processReqDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = processService.updateProcessByProcessKey(processReqDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "根据流程编号获取流程详细信息", notes = "获取流程", produces = "application/json")
    @PostMapping("/getProcessById/{processId}")
    public JsonResult getProcessById(@ApiParam(required = true, name = "流程编号", value = "processId", example = "1") @PathVariable("processId") Long processId) {

        Result<ProcessDTO> result = processService.getProcessById(processId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "根据流程KEY获取流程详细信息", notes = "获取流程", produces = "application/json")
    @PostMapping("/getProcessByKey/{processKey}")
    public JsonResult getProcessByKey(@ApiParam(required = true, name = "流程编号", value = "processKey", example = "pig:processKey") @PathVariable("processKey") String processKey) {

        Result<ProcessDTO> result = processService.getProcessByProcessKey(processKey);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "初始化发起流程表单数据", notes = "初始化发起流程表单数据",  produces = "application/json")
    @PostMapping("/getInitStartFormData/{processKey}")
    public JsonResult getInitStartFormData(@ApiParam(required = true, name = "流程编号", value = "processKey", example = "pig:processKey") @PathVariable("processKey") String processKey) {

        Result<DynamicFormDataDTO> result = processService.getInitStartFormData(processKey);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }


}

