package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.pig.easy.bpm.dto.response.TreeDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.DeptService;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.OrganDeptQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
@RestController
@RequestMapping("/dept")
@Api(tags = "部门管理", value = "部门管理")
public class DeptController extends BaseController {

    @Reference
    DeptService deptService;

    @ApiOperation(value = "获取机构部门树", notes = "获取机构部门树", produces = "application/json")
    @PostMapping("/getOrganUserTree")
    public JsonResult getOrganUserTree(@RequestBody @Valid OrganDeptQueryVO organDeptQueryVO) {

        Result<List<TreeDTO>> result = deptService.getDeptTree(organDeptQueryVO.getParentId(),
                organDeptQueryVO.getCompanyId(),
                organDeptQueryVO.getTenantId());
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

}

