package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/6 17:20
 */
@Data
@ToString
public class CompanyReqDTO extends BaseRequestDTO {

    private static final long serialVersionUID = 4751257108859023497L;

    private Long companyId;

    private String companyCode;

    private Long companyParentId;

    private String companyParentCode;

    private String companyName;

    private String companyAbbr;

    private Integer companyLevel;

    private Integer companyOrder;

    private String companyIcon;

    private String companyUrl;

    private String tenantId;

    private Integer companyStatus;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private Integer pageIndex;

    private Integer pageSize;
}
