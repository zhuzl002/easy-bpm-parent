package com.pig.best.bpm;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.lang.management.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * todo:
 *
 * @author : zhoulin.zhu
 * @date : 2020/12/29 12:19
 */
@Slf4j
public class SystemPropertyTest {

    public static void main(String[] args) {

        System.out.println(Runtime.getRuntime().availableProcessors());
        Map<String, String> getenv = System.getenv();
        for (Map.Entry<String, String> entry : getenv.entrySet()) {
            System.out.println("entrygetKey :   = " + entry.getKey() + ":" + entry.getValue());
        }
        Properties properties = System.getProperties();

        System.out.println("properties = " + properties);

        printUsage();

        getDiskInfo();

        getMemInfo();
    }

    private static void printUsage() {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        System.out.println("operatingSystemMXBean.getName() = " + operatingSystemMXBean.getName());
        System.out.println("operatingSystemMXBean.getArch() = " + operatingSystemMXBean.getArch());
        System.out.println("operatingSystemMXBean.getVersion() = " + operatingSystemMXBean.getVersion());
        System.out.println("operatingSystemMXBean.getAvailableProcessors() = " + operatingSystemMXBean.getAvailableProcessors());

        for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().startsWith("get")
                    && Modifier.isPublic(method.getModifiers())) {
                Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (Exception e) {
                    value = e;
                } // try
                System.out.println(method.getName() + " = " + value);
            } // if
        } // for
    }

    public static void getDiskInfo()
    {
        File[] disks = File.listRoots();
        for(File file : disks)
        {
            System.out.print(file.getPath() + "    ");
            System.out.print("空闲未使用 = " + file.getFreeSpace() / 1024 / 1024 + "M" + "    ");// 空闲空间
            System.out.print("已经使用 = " + file.getUsableSpace() / 1024 / 1024 + "M" + "    ");// 可用空间
            System.out.print("总容量 = " + file.getTotalSpace() / 1024 / 1024 + "M" + "    ");// 总空间
            System.out.println();
        }
    }

    public static void getMemInfo()
    {
        // 获取所有内存管理器MXBean列表，并遍历
        List<MemoryPoolMXBean> memoryPoolMXBeans = ManagementFactory.getMemoryPoolMXBeans();
        for (MemoryPoolMXBean memoryPoolMXBean : memoryPoolMXBeans) {
            // 内存分区名
            String name = memoryPoolMXBean.getName();
            // 内存管理器名称
            String[] memoryManagerNames = memoryPoolMXBean.getMemoryManagerNames();
            // 内存分区类型
            MemoryType type = memoryPoolMXBean.getType();
            // 内存使用情况
            MemoryUsage usage = memoryPoolMXBean.getUsage();
            // 内存使用峰值情况
            MemoryUsage peakUsage = memoryPoolMXBean.getPeakUsage();
            // 打印
            log.info(name + ":");
            log.info("    managers: {}", memoryManagerNames);
            log.info("    type: {}", type.toString());
            log.info("    usage: {}", usage.toString());
            log.info("    peakUsage: {}", peakUsage.toString());
            log.info("");
        }

    }
}
