package com.pig.easy.bpm.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.utils.SnowKeyGenUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.RpcContext;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * todo:
 *
 * @author : zhoulin.zhu
 * @date : 2019/7/23 11:08
 */

@Aspect
@Component
@Slf4j
public class MethodInterceptor {

    private static final String POINT = "execution (* com.pig.easy.bpm.service..*Impl.*(..))";

    @Value("${spring.profiles.active}")
    private String currentEvn;

    @Around(POINT)
    public Object timeAround(ProceedingJoinPoint joinPoint) {

        Object result = null;
        long beginTime = System.currentTimeMillis();

        Object[] args = joinPoint.getArgs();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        String methodName = method.getName();
        Set<Object> allParams = new LinkedHashSet<>();

        for (Object arg : args) {
            if (arg instanceof Map<?, ?>) {
                Map<String, Object> map = (Map<String, Object>) arg;
                allParams.add(map);
            } else {
                allParams.add(arg);
            }
        }

        String requestId = RpcContext.getContext().getObjectAttachments().getOrDefault(BpmConstant.TRACE_ID,SnowKeyGenUtils.getInstance().getNextId()).toString();
        try {
            log.info("request[{}]start，method：{}，param：{}",requestId, methodName, JSONObject.toJSONString(allParams));
        } catch (Exception e) {
            log.info("request[{}]start，method：{}，param：{}",requestId, methodName, allParams);
        }

        try {
            result = joinPoint.proceed(args);
        } catch (Throwable e) {
            log.error("request[{}]end：{}，exception：{} ",requestId, JSONObject.toJSONString(allParams), e);
        }

        long costMs = System.currentTimeMillis() - beginTime;

        try {
            log.info("request[{}]end，method：{}，spendTime：{}ms，result：{}",requestId, methodName, costMs, JSONObject.toJSONString(result));
        } catch (Exception e) {
            log.info("request[{}]end，method：{}，spendTime：{}ms，result：{}",requestId, methodName, costMs, (result));
        }

        return result;
    }

}
