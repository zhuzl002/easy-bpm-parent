package com.pig.easy.bpm.mysql.annotation;

import com.pig.easy.bpm.mysql.reister.DynamicDataSourceRegister;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Documented
@MapperScan
@Target(ElementType.TYPE)
@Import(DynamicDataSourceRegister.class)
public @interface EnableMysql {

    String dataSource() default "common";

    @AliasFor(annotation = MapperScan.class, attribute = "basePackages")
    String[] scanBasePackages() default {};

    @AliasFor(annotation = MapperScan.class, attribute = "basePackageClasses")
    Class<?>[] scanBasePackageClasses() default {};
}
