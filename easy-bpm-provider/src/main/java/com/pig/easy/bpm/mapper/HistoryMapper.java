package com.pig.easy.bpm.mapper;

import com.pig.easy.bpm.entity.HistoryDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 审批历史表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-07-01
 */
@Mapper
public interface HistoryMapper extends BaseMapper<HistoryDO> {

}
