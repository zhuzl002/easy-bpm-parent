package com.pig.easy.bpm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.MessageContentQueryDTO;
import com.pig.easy.bpm.dto.request.MessageContentSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.MessageContentDTO;
import com.pig.easy.bpm.entity.MessageContentDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.MessageContentMapper;
import com.pig.easy.bpm.service.MessageContentService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
 * <p>
 * 通知内容表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
@Service
public class MessageContentServiceImpl extends BeseServiceImpl<MessageContentMapper, MessageContentDO>implements MessageContentService {

        @Autowired
        MessageContentMapper mapper;

        @Override
        public Result<PageInfo<MessageContentDTO>>getListByCondition(MessageContentQueryDTO param){

          if(param==null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }
          int pageIndex=CommonUtils.evalInt(param.getPageIndex(),DEFAULT_PAGE_INDEX);
          int pageSize=CommonUtils.evalInt(param.getPageSize(),DEFAULT_PAGE_SIZE);

          PageHelper.startPage(pageIndex,pageSize);
          param.setValidState(VALID_STATE);
          List<MessageContentDTO>list=mapper.getListByCondition(param);
          if(list==null){
           list=new ArrayList<>();
          }
          PageInfo<MessageContentDTO>pageInfo=new PageInfo<>(list);
          return Result.responseSuccess(pageInfo);
        }


        @Override
        public Result<Integer>insertMessageContent(MessageContentSaveOrUpdateDTO param){

          if(param==null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageContentDO temp=BeanUtils.switchToDO(param, MessageContentDO.class);
            Integer num=mapper.insert(temp);
            return Result.responseSuccess(num);
        }

         @Override
         public Result<Integer>updateMessageContent(MessageContentSaveOrUpdateDTO param){

          if(param==null){
           return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageContentDO temp=BeanUtils.switchToDO(param, MessageContentDO.class);
          Integer num=mapper.updateById(temp);
          return Result.responseSuccess(num);
        }

        @Override
        public Result<Integer>deleteMessageContent(MessageContentSaveOrUpdateDTO param){

          if(param==null){
             return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageContentDO temp=BeanUtils.switchToDO(param, MessageContentDO.class);
          temp.setValidState(INVALID_STATE);
          Integer num=mapper.updateById(temp);
          return Result.responseSuccess(num);
        }

    @Override
    public Result<MessageContentDTO> getMessageContent(MessageContentQueryDTO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getMessagePlatform());
        BestBpmAsset.isAssetEmpty(param.getMessageTypeCode());

        Integer processId = param.getProcessId();
        if (CommonUtils.evalInt(processId) <= 0) {
            param.setProcessId(0);
            processId = 0;
        }
        List<Integer> processIdList = new ArrayList<>();
        processIdList.add(param.getProcessId());

        if (!processIdList.contains(0)) {
            processIdList.add(0);
        }

        MessageContentDO messageContentDO = BeanUtils.switchToDO(param, MessageContentDO.class);
        messageContentDO.setEventCodes(null);
        messageContentDO.setProcessId(null);
        List<MessageContentDO> notifyContentDOList = mapper.selectList(new QueryWrapper<>(messageContentDO)
                .in("event_codes", param.getEventCodes())
                .in("process_id", processIdList));

        return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
    }

    @Override
    public Result<MessageContentDTO> getMessageContent(Integer processId, String tenantId, String eventCode,String messageTypeCode, String messagePlatform) {

        MessageContentQueryDTO param = new MessageContentQueryDTO();
        param.setProcessId(processId);
        param.setTenantId(tenantId);
        param.setEventCodes(eventCode);
        param.setMessagePlatform(messagePlatform);
        param.setMessageTypeCode(messageTypeCode);

        return getMessageContent(param);
    }

}
