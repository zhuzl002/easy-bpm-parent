package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.dto.request.DictQueryDTO;
import com.pig.easy.bpm.dto.response.DictDTO;
import com.pig.easy.bpm.entity.DictDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@Mapper
public interface DictMapper extends BaseMapper<DictDO> {

    List<DictDTO> getListByCondition(DictQueryDTO dictQueryDTO);
}
